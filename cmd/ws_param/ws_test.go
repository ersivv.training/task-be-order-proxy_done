package main

import (
	"github.com/gorilla/websocket"
	"github.com/stretchr/testify/assert"
	"net/http/httptest"
	"strings"
	"testing"
)

func Test_echo(t *testing.T) {
	type args struct {
		message string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "blank",
			args: args{
				message: "blank",
			},
			want: "blank",
		},
		{
			name: "three test",
			args: args{
				message: "test test test",
			},
			want: "test test test",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := httptest.NewServer(echoHandler(tt.args.message))
			defer s.Close()

			// Convert http://127.0.0.1 to ws://127.0.0.
			u := "ws" + strings.TrimPrefix(s.URL, "http")

			// Connect to the server
			ws, _, err := websocket.DefaultDialer.Dial(u, nil)
			if err != nil {
				t.Fatalf("%v", err)
			}
			defer ws.Close()

			// Send message to server, read response and check to see if it's what we expect.
			if err := ws.WriteMessage(websocket.TextMessage, []byte("hello")); err != nil {
				t.Fatalf("%v", err)
			}
			_, p, err := ws.ReadMessage()
			if err != nil {
				t.Fatalf("%v", err)
			}
			assert.Equal(t, tt.want, string(p), "bad message")
		})
	}
}
