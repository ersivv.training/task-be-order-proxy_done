package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"testtask/pkg/protocol"
	"time"

	"github.com/gorilla/websocket"
)

var (
	addrServer                   = flag.String("server", "localhost:8080", "http business service address")
	addrListen                   = flag.String("listen", "localhost:8081", "http proxy service address")
	LimitOrderCountByInstrument  = flag.Int("count", 5, "limit count opened orders per client per instrument at the moment of time")
	LimitOrderVolumeByInstrument = flag.Float64("volume", 400, "limit sum of volumes of opened orders per client per instrument at the moment of time")

	upgrader = websocket.Upgrader{}
	timeout  = 10 * time.Second
)

func main() {

	http.HandleFunc("/connect", connectHandler(*addrServer, *LimitOrderVolumeByInstrument, *LimitOrderCountByInstrument))
	log.Printf("Waiting for connections on %s/connect", *addrListen)
	log.Fatal(http.ListenAndServe(*addrListen, nil))

}

type client struct {
	readClient  chan protocol.OrderRequest
	writeClient chan protocol.OrderResponse
	readServer  chan protocol.OrderResponse
	writeServer chan protocol.OrderRequest

	errorsChan chan error

	orderVolumeById     map[uint32]float64
	orderInstrumentById map[uint32]string

	orderVolumeByInstrument map[string]float64
	orderCountByInstrument  map[string]int
}

func connectHandler(addrServer string, volume float64, count int) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		wsClient, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Print("upgrade:", err)
			return
		}
		defer wsClient.Close()

		var wsServer *websocket.Conn
		if addrServer != "" {
			wsServerUrl := url.URL{Scheme: "ws", Host: addrServer, Path: "/connect"}
			log.Printf("connecting to %s", wsServerUrl.String())

			wsServer, _, err = websocket.DefaultDialer.Dial(wsServerUrl.String(), nil)
			if err != nil {
				log.Print("failed connect to server:", err)
				return
			}
			defer wsServer.Close()
		}

		client := client{
			readClient:  make(chan protocol.OrderRequest),
			writeClient: make(chan protocol.OrderResponse),
			readServer:  make(chan protocol.OrderResponse),
			writeServer: make(chan protocol.OrderRequest),

			errorsChan: make(chan error, 1),

			orderVolumeById:     make(map[uint32]float64),
			orderInstrumentById: make(map[uint32]string),

			orderVolumeByInstrument: make(map[string]float64),
			orderCountByInstrument:  make(map[string]int),
		}

		go client.readClientWorker(wsClient)
		go client.readServerWorker(wsServer)

		go client.writeClientWorker(wsClient)
		go client.writeServerWorker(wsServer)

		client.process(volume, count)
	}
}

func (c *client) readClientWorker(ws *websocket.Conn) {
	for {
		ws.SetReadDeadline(time.Now().Add(timeout))
		_, msg, err := ws.ReadMessage()
		if err != nil {
			log.Println("err", err)
			return
		}
		req := protocol.DecodeOrderRequest(msg)
		log.Printf("client req: %v", req)

		c.readClient <- req
	}
}

func (c *client) readServerWorker(ws *websocket.Conn) {
	if ws == nil {
		return
	}

	for {
		ws.SetReadDeadline(time.Now().Add(timeout))
		_, msg, err := ws.ReadMessage()
		if err != nil {
			log.Println("err", err)
			return
		}
		req := protocol.DecodeOrderResponse(msg)
		log.Printf("server resp: %v", req)

		c.readServer <- req
	}
}

func (c *client) writeClientWorker(ws *websocket.Conn) {
	for msg := range c.writeClient {
		ws.SetWriteDeadline(time.Now().Add(timeout))
		if err := ws.WriteMessage(websocket.TextMessage, protocol.EncodeOrderResponse(msg)); err != nil {
			c.errorsChan <- err
		}
	}
}

func (c *client) writeServerWorker(ws *websocket.Conn) {
	for msg := range c.writeServer {
		if ws == nil {
			return
		}
		ws.SetWriteDeadline(time.Now().Add(timeout))
		if err := ws.WriteMessage(websocket.TextMessage, protocol.EncodeOrderRequest(msg)); err != nil {
			c.errorsChan <- err
		}
	}
}

func (c *client) process(volume float64, count int) {
	defer close(c.readClient)
	defer close(c.writeClient)
	defer close(c.readServer)
	defer close(c.writeServer)

LOOP:
	for {
		select {
		case err := <-c.errorsChan:
			log.Printf("error c.errorsChan: %+v\n", err)
			return
		default:
		}

		select {
		case err := <-c.errorsChan:
			log.Printf("error c.errorsChan: %+v\n", err)
			break LOOP
		case req := <-c.readClient:
			if ok, code := c.checkLogic(req, volume, count); !ok {
				resp := newResponse(req.ID, code)
				log.Printf("proxy resp: %v", resp)
				c.writeClient <- resp
				continue
			}
			c.writeServer <- req
		case resp := <-c.readServer:
			if err := c.rollbackOrConfirm(resp); err != nil {
				log.Println("rollbackOrConfirm error: ", err)
			}
			c.writeClient <- resp
		default:
		}

	}

}

func newResponse(id uint32, code uint16) protocol.OrderResponse {
	return protocol.OrderResponse{
		ID:   id,
		Code: code,
	}
}

func (c *client) checkLogic(req protocol.OrderRequest, volume float64, count int) (bool, uint16) {
	if req.ReqType == 0 || req.ReqType > 2 {
		return false, 3
	}

	if req.OrderKind == 0 || req.OrderKind > 2 {
		return false, 3
	}

	//Order with req_type = 2 (close order) send direct to server
	if req.ReqType == 2 {
		return true, 0
	}

	if cnt, exist := c.orderCountByInstrument[req.Instrument]; exist {
		if cnt >= count {
			return false, 1
		}
	}

	if vol, exist := c.orderVolumeByInstrument[req.Instrument]; exist {
		if vol+req.Volume >= volume {
			return false, 2
		}
	}

	c.orderVolumeById[req.ID] = req.Volume
	c.orderInstrumentById[req.ID] = req.Instrument

	c.orderCountByInstrument[req.Instrument]++
	c.orderVolumeByInstrument[req.Instrument] += req.Volume

	return true, 0
}

//TODO: What to do, if OrderID not exist in cache proxy?!
func (c *client) rollbackOrConfirm(resp protocol.OrderResponse) error {

	var (
		volume     float64
		instrument string
		exist      bool
	)

	if volume, exist = c.orderVolumeById[resp.ID]; !exist {
		return fmt.Errorf("not exist c.orderVolumeById[%d]", resp.ID)
	}

	if instrument = c.orderInstrumentById[resp.ID]; !exist {
		return fmt.Errorf("not exist orderInstrumentById[%d]", resp.ID)
	}

	if _, ok := c.orderVolumeByInstrument[instrument]; !ok {
		return fmt.Errorf("not exist orderVolumeByInstrument[%s]", instrument)
	}
	c.orderVolumeByInstrument[instrument] -= volume

	if _, ok := c.orderCountByInstrument[instrument]; !ok {
		return fmt.Errorf("not exist orderCountByInstrument[%s]", instrument)
	}
	c.orderCountByInstrument[instrument]--

	delete(c.orderVolumeById, resp.ID)
	delete(c.orderInstrumentById, resp.ID)

	return nil
}
