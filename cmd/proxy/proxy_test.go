package main

import (
	"testing"
	"testtask/pkg/protocol"

	"github.com/stretchr/testify/assert"
)

func Test_client_checkLogic(t *testing.T) {

	type fields struct {
		orderVolumeById         map[uint32]float64
		orderInstrumentById     map[uint32]string
		orderVolumeByInstrument map[string]float64
		orderCountByInstrument  map[string]int
	}
	type args struct {
		req    protocol.OrderRequest
		volume float64
		count  int
	}
	tests := []struct {
		name       string
		fields     fields
		args       args
		want       bool
		wantCode   uint16
		wantClient client
	}{
		{
			name: "good send server",
			fields: fields{
				orderVolumeById: map[uint32]float64{
					1000: 100,
				},
				orderInstrumentById: map[uint32]string{
					1000: "EURUSD",
				},
				orderVolumeByInstrument: map[string]float64{
					"EURUSD": 100,
				},
				orderCountByInstrument: map[string]int{
					"EURUSD": 1,
				},
			},
			args: args{
				req: protocol.OrderRequest{
					ID:         1001,
					ReqType:    1,
					OrderKind:  1,
					Volume:     200,
					Instrument: "EURUSD",
				},
				volume: 500,
				count:  10,
			},
			want:     true,
			wantCode: 0,
			wantClient: client{
				orderVolumeById: map[uint32]float64{
					1000: 100,
					1001: 200,
				},
				orderInstrumentById: map[uint32]string{
					1000: "EURUSD",
					1001: "EURUSD",
				},
				orderVolumeByInstrument: map[string]float64{
					"EURUSD": 300,
				},
				orderCountByInstrument: map[string]int{
					"EURUSD": 2,
				},
			},
		},
		{
			name: "send request order close",
			fields: fields{
				orderVolumeById: map[uint32]float64{
					1000: 100,
				},
				orderInstrumentById: map[uint32]string{
					1000: "EURUSD",
				},
				orderVolumeByInstrument: map[string]float64{
					"EURUSD": 100,
				},
				orderCountByInstrument: map[string]int{
					"EURUSD": 1,
				},
			},
			args: args{
				req: protocol.OrderRequest{
					ID:         1001,
					ReqType:    2,
					OrderKind:  1,
					Volume:     100,
					Instrument: "EURUSD",
				},
				volume: 500,
				count:  10,
			},
			want:     true,
			wantCode: 0,
			wantClient: client{
				orderVolumeById: map[uint32]float64{
					1000: 100,
				},
				orderInstrumentById: map[uint32]string{
					1000: "EURUSD",
				},
				orderVolumeByInstrument: map[string]float64{
					"EURUSD": 100,
				},
				orderCountByInstrument: map[string]int{
					"EURUSD": 1,
				},
			},
		},
		{
			name: "overflow count",
			fields: fields{
				orderVolumeById: map[uint32]float64{
					1000: 100,
					1001: 100,
					1002: 100,
					1003: 100,
					1004: 100,
				},
				orderInstrumentById: map[uint32]string{
					1000: "EURUSD",
					1001: "EURUSD",
					1002: "EURUSD",
					1003: "EURUSD",
					1004: "EURUSD",
				},
				orderVolumeByInstrument: map[string]float64{
					"EURUSD": 500,
				},
				orderCountByInstrument: map[string]int{
					"EURUSD": 5,
				},
			},
			args: args{
				req: protocol.OrderRequest{
					ID:         1006,
					ReqType:    1,
					OrderKind:  1,
					Volume:     200,
					Instrument: "EURUSD",
				},
				volume: 500,
				count:  5,
			},
			want:     false,
			wantCode: 1,
			wantClient: client{
				orderVolumeById: map[uint32]float64{
					1000: 100,
					1001: 100,
					1002: 100,
					1003: 100,
					1004: 100,
				},
				orderInstrumentById: map[uint32]string{
					1000: "EURUSD",
					1001: "EURUSD",
					1002: "EURUSD",
					1003: "EURUSD",
					1004: "EURUSD",
				},
				orderVolumeByInstrument: map[string]float64{
					"EURUSD": 500,
				},
				orderCountByInstrument: map[string]int{
					"EURUSD": 5,
				},
			},
		},
		{
			name: "overflow volume",
			fields: fields{
				orderVolumeById: map[uint32]float64{
					1000: 100,
					1001: 100,
					1002: 100,
					1003: 100,
					1004: 100,
				},
				orderInstrumentById: map[uint32]string{
					1000: "EURUSD",
					1001: "EURUSD",
					1002: "EURUSD",
					1003: "EURUSD",
					1004: "EURUSD",
				},
				orderVolumeByInstrument: map[string]float64{
					"EURUSD": 500,
				},
				orderCountByInstrument: map[string]int{
					"EURUSD": 5,
				},
			},
			args: args{
				req: protocol.OrderRequest{
					ID:         1006,
					ReqType:    1,
					OrderKind:  1,
					Volume:     200,
					Instrument: "EURUSD",
				},
				volume: 500,
				count:  10,
			},
			want:     false,
			wantCode: 2,
			wantClient: client{
				orderVolumeById: map[uint32]float64{
					1000: 100,
					1001: 100,
					1002: 100,
					1003: 100,
					1004: 100,
				},
				orderInstrumentById: map[uint32]string{
					1000: "EURUSD",
					1001: "EURUSD",
					1002: "EURUSD",
					1003: "EURUSD",
					1004: "EURUSD",
				},
				orderVolumeByInstrument: map[string]float64{
					"EURUSD": 500,
				},
				orderCountByInstrument: map[string]int{
					"EURUSD": 5,
				},
			},
		},
		{
			name: "reject incorrect req_type 0",
			fields: fields{
				orderVolumeById: map[uint32]float64{
					1000: 100,
				},
				orderInstrumentById: map[uint32]string{
					1000: "EURUSD",
				},
				orderVolumeByInstrument: map[string]float64{
					"EURUSD": 100,
				},
				orderCountByInstrument: map[string]int{
					"EURUSD": 1,
				},
			},
			args: args{
				req: protocol.OrderRequest{
					ID:         1006,
					ReqType:    0,
					OrderKind:  1,
					Volume:     200,
					Instrument: "EURUSD",
				},
				volume: 500,
				count:  10,
			},
			want:     false,
			wantCode: 3,
			wantClient: client{
				orderVolumeById: map[uint32]float64{
					1000: 100,
				},
				orderInstrumentById: map[uint32]string{
					1000: "EURUSD",
				},
				orderVolumeByInstrument: map[string]float64{
					"EURUSD": 100,
				},
				orderCountByInstrument: map[string]int{
					"EURUSD": 1,
				},
			},
		},
		{
			name: "reject incorrect req_type 10",
			fields: fields{
				orderVolumeById: map[uint32]float64{
					1000: 100,
				},
				orderInstrumentById: map[uint32]string{
					1000: "EURUSD",
				},
				orderVolumeByInstrument: map[string]float64{
					"EURUSD": 100,
				},
				orderCountByInstrument: map[string]int{
					"EURUSD": 1,
				},
			},
			args: args{
				req: protocol.OrderRequest{
					ID:         1006,
					ReqType:    10,
					OrderKind:  1,
					Volume:     200,
					Instrument: "EURUSD",
				},
				volume: 500,
				count:  10,
			},
			want:     false,
			wantCode: 3,
			wantClient: client{
				orderVolumeById: map[uint32]float64{
					1000: 100,
				},
				orderInstrumentById: map[uint32]string{
					1000: "EURUSD",
				},
				orderVolumeByInstrument: map[string]float64{
					"EURUSD": 100,
				},
				orderCountByInstrument: map[string]int{
					"EURUSD": 1,
				},
			},
		},
		{
			name: "reject incorrect order_kind 0",
			fields: fields{
				orderVolumeById: map[uint32]float64{
					1000: 100,
				},
				orderInstrumentById: map[uint32]string{
					1000: "EURUSD",
				},
				orderVolumeByInstrument: map[string]float64{
					"EURUSD": 100,
				},
				orderCountByInstrument: map[string]int{
					"EURUSD": 1,
				},
			},
			args: args{
				req: protocol.OrderRequest{
					ID:         1006,
					ReqType:    1,
					OrderKind:  0,
					Volume:     200,
					Instrument: "EURUSD",
				},
				volume: 500,
				count:  10,
			},
			want:     false,
			wantCode: 3,
			wantClient: client{
				orderVolumeById: map[uint32]float64{
					1000: 100,
				},
				orderInstrumentById: map[uint32]string{
					1000: "EURUSD",
				},
				orderVolumeByInstrument: map[string]float64{
					"EURUSD": 100,
				},
				orderCountByInstrument: map[string]int{
					"EURUSD": 1,
				},
			},
		},
		{
			name: "reject incorrect order_kind 10",
			fields: fields{
				orderVolumeById: map[uint32]float64{
					1000: 100,
				},
				orderInstrumentById: map[uint32]string{
					1000: "EURUSD",
				},
				orderVolumeByInstrument: map[string]float64{
					"EURUSD": 100,
				},
				orderCountByInstrument: map[string]int{
					"EURUSD": 1,
				},
			},
			args: args{
				req: protocol.OrderRequest{
					ID:         1006,
					ReqType:    1,
					OrderKind:  10,
					Volume:     200,
					Instrument: "EURUSD",
				},
				volume: 500,
				count:  10,
			},
			want:     false,
			wantCode: 3,
			wantClient: client{
				orderVolumeById: map[uint32]float64{
					1000: 100,
				},
				orderInstrumentById: map[uint32]string{
					1000: "EURUSD",
				},
				orderVolumeByInstrument: map[string]float64{
					"EURUSD": 100,
				},
				orderCountByInstrument: map[string]int{
					"EURUSD": 1,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &client{
				orderVolumeById:         tt.fields.orderVolumeById,
				orderInstrumentById:     tt.fields.orderInstrumentById,
				orderVolumeByInstrument: tt.fields.orderVolumeByInstrument,
				orderCountByInstrument:  tt.fields.orderCountByInstrument,
			}
			ok, code := c.checkLogic(tt.args.req, tt.args.volume, tt.args.count)
			// if ok != tt.want {
			// 	t.Errorf("client.checkLogic() ok = %v, want %v", ok, tt.want)
			// }
			// if code != tt.wantCode {
			// 	t.Errorf("client.checkLogic() code = %v, wantCode %v", code, tt.wantCode)
			// }
			// if !reflect.DeepEqual(c, tt.wantClient) {
			// 	t.Errorf("client.checkLogic()\nretr client = %+v\nwant client = %+v", *c, tt.wantClient)
			// }
			assert.Equal(t, ok, tt.want, "Not equal return result")
			assert.Equal(t, code, tt.wantCode, "Not equal return code")
			assert.Equal(t, *c, tt.wantClient, "Not equal client")
		})
	}
}

func Test_client_rollbackOrConfirm(t *testing.T) {
	type fields struct {
		orderVolumeById         map[uint32]float64
		orderInstrumentById     map[uint32]string
		orderVolumeByInstrument map[string]float64
		orderCountByInstrument  map[string]int
	}
	type args struct {
		resp protocol.OrderResponse
	}
	tests := []struct {
		name       string
		fields     fields
		args       args
		want       error
		wantClient client
	}{
		{
			name: "good response server",
			fields: fields{
				orderVolumeById: map[uint32]float64{
					1000: 100,
					1001: 200,
				},
				orderInstrumentById: map[uint32]string{
					1000: "EURUSD",
					1001: "EURUSD",
				},
				orderVolumeByInstrument: map[string]float64{
					"EURUSD": 300,
				},
				orderCountByInstrument: map[string]int{
					"EURUSD": 2,
				},
			},
			args: args{
				resp: protocol.OrderResponse{
					ID:   1000,
					Code: 0,
				},
			},
			want: nil,
			wantClient: client{
				orderVolumeById: map[uint32]float64{
					1001: 200,
				},
				orderInstrumentById: map[uint32]string{
					1001: "EURUSD",
				},
				orderVolumeByInstrument: map[string]float64{
					"EURUSD": 200,
				},
				orderCountByInstrument: map[string]int{
					"EURUSD": 1,
				},
			},
		},
		{
			name: "response with code other error",
			fields: fields{
				orderVolumeById: map[uint32]float64{
					1000: 100,
					1001: 200,
				},
				orderInstrumentById: map[uint32]string{
					1000: "EURUSD",
					1001: "EURUSD",
				},
				orderVolumeByInstrument: map[string]float64{
					"EURUSD": 300,
				},
				orderCountByInstrument: map[string]int{
					"EURUSD": 2,
				},
			},
			args: args{
				resp: protocol.OrderResponse{
					ID:   1000,
					Code: 3,
				},
			},
			want: nil,
			wantClient: client{
				orderVolumeById: map[uint32]float64{
					1001: 200,
				},
				orderInstrumentById: map[uint32]string{
					1001: "EURUSD",
				},
				orderVolumeByInstrument: map[string]float64{
					"EURUSD": 200,
				},
				orderCountByInstrument: map[string]int{
					"EURUSD": 1,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &client{
				orderVolumeById:         tt.fields.orderVolumeById,
				orderInstrumentById:     tt.fields.orderInstrumentById,
				orderVolumeByInstrument: tt.fields.orderVolumeByInstrument,
				orderCountByInstrument:  tt.fields.orderCountByInstrument,
			}
			err := c.rollbackOrConfirm(tt.args.resp)
			if err != tt.want {
				t.Errorf("Unexpected error: %v (want %v)", err, tt.want)
			}

			assert.Equal(t, *c, tt.wantClient, "Not equal client")

		})
	}
}

// func Test_connectHandler(t *testing.T) {
// 	type args struct {
// 		addrServer string
// 		volume     float64
// 		count      int
// 		req        protocol.OrderRequest
// 	}
// 	tests := []struct {
// 		name string
// 		args args
// 		want protocol.OrderResponse
// 	}{
// 		{
// 			name: "web",
// 			args: args{
// 				addrServer: "",
// 				volume:     500,
// 				count:      5,
// 				req: protocol.OrderRequest{
// 					ID:         1001,
// 					ReqType:    2,
// 					OrderKind:  1,
// 					Volume:     100,
// 					Instrument: "EURUSD",
// 				},
// 			},
// 			want: protocol.OrderResponse{
// 				ID:   1001,
// 				Code: 3,
// 			},
// 		},
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			s := httptest.NewServer(http.HandlerFunc(connectHandler(tt.args.addrServer, tt.args.volume, tt.args.count)))
// 			defer s.Close()

// 			u := "ws" + strings.TrimPrefix(s.URL, "http")
// 			ws, _, err := websocket.DefaultDialer.Dial(u, nil)
// 			if err != nil {
// 				t.Fatalf("%v", err)
// 			}
// 			defer ws.Close()

// 			sendMessage(t, ws, tt.args.req)

// 			resp := receiveWSMessage(t, ws)
// 			assert.Equal(t, tt.want, resp, "serf")

// 		})
// 	}
// }

// func sendMessage(t *testing.T, ws *websocket.Conn, req protocol.OrderRequest) {
// 	t.Helper()

// 	ws.SetWriteDeadline(time.Now().Add(timeout))
// 	if err := ws.WriteMessage(websocket.TextMessage, protocol.EncodeOrderRequest(req)); err != nil {
// 		t.Fatalf("%v", err)
// 	}
// }

// func receiveWSMessage(t *testing.T, ws *websocket.Conn) protocol.OrderResponse {
// 	t.Helper()

// 	ws.SetReadDeadline(time.Now().Add(timeout))
// 	_, m, err := ws.ReadMessage()
// 	if err != nil {
// 		t.Fatalf("%v", err)
// 	}

// 	return protocol.DecodeOrderResponse(m)
// }
